Replace the .apk application file located under with yours:
application/



================================================
In config.properties file replace the following entries with relevant data about the application under testing:
fileNameAPK
appPackage
appActivity



================================================
Run whole set related to runner
mvn test -Dtest=com.zemoga.runner.RunSmokeTest cluecumber-report:reporting



================================================
Run specific set of test cases (change the tag according with the testing needs):
mvn test -Dtest=com.zemoga.runner.RunSmokeTest -Dcucumber.options="--tags @AppiumAndroid" cluecumber-report:reporting



================================================
Cluecumber report generated in:
target/generated-report/index.html