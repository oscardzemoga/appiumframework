package com.zemoga.steps;

import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;
import com.zemoga.page.LandingPage;
import org.testng.Assert;

/**
 * This class provides the bridge between feature files and Java execution.
 */
public class Stepdefs {

	private WebDriver driver = null;
	private LandingPage landingPage = null;

	public Stepdefs(){
		driver = BaseTest.getDriver();
	}

	@Given("^I am in landing page$")
    public void i_am_in_landing_page() {
		landingPage = new LandingPage(driver);
		landingPage.getIntoLoginPage();
		Assert.assertTrue(true, "nothing was good");
    }

	@Given("^I am in landing page hidden mode$")
	public void i_am_in_landing_page_hidden_mode() {
		landingPage = new LandingPage(driver);
		landingPage.getIntoLoginPage();
		Assert.assertTrue(false, "nothing was good");
	}
}