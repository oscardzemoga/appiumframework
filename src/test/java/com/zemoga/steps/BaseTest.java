package com.zemoga.steps;

import com.zemoga.driver.MyDriver;
import com.zemoga.util.AppiumServer;
import com.zemoga.util.SystemUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

/**
 * This class provides util callings to init drivers as needed: Local, STF, Saucelabs in the future.
 *
 * Also it's in charge of teardown driver and Appium server.
 *
 * endTest() function should be called in some point to report test cases execution status on TestRail.
 */
public class BaseTest {

    protected MyDriver driverFactory = null;
    protected static WebDriver driver = null;

    @Before("@AppiumAndroid")
    public void AppiumAndroid () {
        driverFactory = new MyDriver("AppiumAndroid");
        driver = driverFactory.getDriver();
    }

    @Before("@AppiumAndroid_STF")
    public void AppiumAndroid_STF () {
        driverFactory = new MyDriver("AppiumAndroid_STF");
        driver = driverFactory.getDriver();
    }

    public static WebDriver getDriver(){
        return driver;
    }

    @After
    public void teardown () {
        AppiumServer.stop();
        if(driverFactory.isRemote()) {
            try {
                driverFactory.releaseStfDevice();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    //@After
    public void endTest(Scenario scenario) {

        if(scenario.isFailed()){
            final byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        }

        //if(this.sessionId!=null)
        //    sauceUtils.updateResults(!scenario.isFailed(), sessionId);

        int testResult = 0;
        String testCaseID = null;

        if(scenario.isFailed())
            testResult = 5;
        else
            testResult = 1;

        testCaseID = scenario.getName().split("-")[0];
        SystemUtils systemUtils = new SystemUtils();
        systemUtils.updateTestCaseResult(testCaseID, testResult);

        if(driver!=null){
            driver.close();
            driver.quit();
        }
    }
}