package com.zemoga.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.annotations.*;

@CucumberOptions(	plugin = {"pretty","html:target/report","json:target/cucumber-report/cucumber.json"},
					features = {"src/test/resources"},
					glue = {"classpath:com/zemoga/steps"},
					tags = {"@AppiumAndroid"}) //Most important here! As you can decide what tests to run

public class RunSmokeTest extends AbstractTestNGCucumberTests{

	private TestNGCucumberRunner testNGCucumberRunner;

	@BeforeClass(alwaysRun = true)
	public void setUpClass() {
		testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
	}

	@Test(groups = "cucumber", description = "Runs Cucumber Feature", dataProvider = "features")
	public void feature(CucumberFeatureWrapper cucumberFeature) {
		testNGCucumberRunner.runCucumber(cucumberFeature.getCucumberFeature());
	}

	@DataProvider
	public Object[][] features() {
		return testNGCucumberRunner.provideFeatures();
	}

	@AfterClass(alwaysRun = true)
	public void tearDownClass() {
		testNGCucumberRunner.finish();
	}

	@AfterTest
	public void tearDownTest() {
		System.out.println("closed");
	}
}