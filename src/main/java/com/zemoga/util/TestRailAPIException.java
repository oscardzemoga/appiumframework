package com.zemoga.util;


/**
 * This class provides exception handing when framework wants to use TestRail.
 */
public class TestRailAPIException extends Exception
{
	public TestRailAPIException(String message)
	{
		super(message);
	}
}