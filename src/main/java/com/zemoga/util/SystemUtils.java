package com.zemoga.util;

import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.json.simple.JSONObject;


/**
 * This class provides a set of helpers to manage testing process.
 */
public class SystemUtils {
	
	private TestRailAPIClient client;
	private String idRunTestRail;
	
	public String readEnvVariable(String var) {
		
		if(var.length() > 0) {
			return System.getenv(var);
		}
		else
			return null; 
	}
	
	public String readPropertyKey(String key) {
		InputStream filesReader = null;
		Properties prop = null;
		
		try {
			filesReader = new FileInputStream("config.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		prop = new Properties();
		
		try {
			prop.load(filesReader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return prop.getProperty(key);
	}
	
	public void connectTestRail(String user, String passwd) {
		client = new TestRailAPIClient(readPropertyKey("testRailBaseURL"));
		client.setUser(user);
		client.setPassword(passwd);
	}
	
	public void createRun(String projectID, String suiteID) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");  
		LocalDateTime now = LocalDateTime.now();  
		
		JSONObject c;
		Map<String, String> data = new HashMap<String, String>();
		data.put("suite_id", suiteID);
		data.put("name", "Automated Suite "+dtf.format(now));
		
		try {
			c = (JSONObject) client.sendPost("add_run/"+projectID, data);
			this.idRunTestRail = c.get("id").toString();
		} catch (IOException | TestRailAPIException e) {
			e.printStackTrace();
		}
	}
	
	public void updateTestCaseResult(String testCaseID, int result) {
		Map data = new HashMap();
		data.put("status_id", result);
		//Just a generic comment, it can be improved
		data.put("comment", "This test worked fine!");
		try {
			JSONObject r = (JSONObject) client.sendPost("add_result_for_case/"+idRunTestRail+"/"+testCaseID, data);
		} catch (IOException | TestRailAPIException e) {
			e.printStackTrace();
		}
	}
}