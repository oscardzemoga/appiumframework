package com.zemoga.driver;

import com.zemoga.STF.DeviceApi;
import com.zemoga.STF.STFService;
import com.zemoga.util.AppiumServer;
import com.zemoga.util.SystemUtils;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * This class provides the driver to connect and manipulate devices. They can be
 * local and also STF devices
 */
public class MyDriver {

	private WebDriver driver = null;
	private final String STF_SERVICE_URL = "http://localhost:7100";
	//Move token to properties
	private final String ACCESS_TOKEN = "4dea8117e48f4d7db9258557e3baf8460ad733c6f1e546aa9a4c7700bc623768";
	private DeviceApi deviceApi;
	private boolean isRemote = false;
	private String deviceName = null;

	@SuppressWarnings("DuplicateBranchesInSwitch")
	public MyDriver(String device){

		SystemUtils sysUtils = new SystemUtils();
		String URLAppium = null;
		File classpathRoot = new File(System.getProperty("user.dir"));
		File appDir = new File(classpathRoot	, "/application/");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		String appFileName = sysUtils.readPropertyKey("fileNameAPK");
		File app = new File(appDir, appFileName);
		String port = null;
		deviceName = sysUtils.readPropertyKey("Address_device1");

		switch(device){
			case "AppiumAndroid":

				port = sysUtils.readPropertyKey("AppiumPort_device1");
				URLAppium = "http://127.0.0.1:"+port+"/wd/hub";

				capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
				capabilities.setCapability("deviceName", deviceName);
				capabilities.setCapability("platformVersion", sysUtils.readPropertyKey("PlatformVersion1") );
				capabilities.setCapability("platformName", "Android");
				capabilities.setCapability("app", app.getAbsolutePath());
				capabilities.setCapability("automationName", "UiAutomator2");
				capabilities.setCapability("noReset", true);
				capabilities.setCapability("appPackage", sysUtils.readPropertyKey("appPackage"));
				capabilities.setCapability("appActivity", sysUtils.readPropertyKey("appActivity"));

				try {
					AppiumServer.start(new Integer(port));
					driver = new AndroidDriver(new URL(URLAppium), capabilities);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			break;

			case "AppiumAndroid_STF":

				port = sysUtils.readPropertyKey("AppiumPort_device1");
				URLAppium = "http://127.0.0.1:"+port+"/wd/hub";

				capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
				capabilities.setCapability("deviceName", deviceName);
				capabilities.setCapability("platformVersion", sysUtils.readPropertyKey("PlatformVersion1"));
				capabilities.setCapability("platformName", "Android");
				capabilities.setCapability("app", app.getAbsolutePath());
				capabilities.setCapability("automationName", "UiAutomator2");
				capabilities.setCapability("noReset", true);
				capabilities.setCapability("appPackage", sysUtils.readPropertyKey("appPackage"));
				capabilities.setCapability("appActivity", sysUtils.readPropertyKey("appActivity"));

				try {
					connectToStfDevice(sysUtils.readPropertyKey("Address_device1"));
					AppiumServer.start(new Integer(port));
					driver = new AndroidDriver(new URL(URLAppium), capabilities);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
				break;

			default:
				throw new IllegalStateException("Unexpected value: " + device);
		}

	}
	
	public WebDriver getDriver(){
		return this.driver;
	}

	private void connectToStfDevice(String deviceSerial) throws MalformedURLException, URISyntaxException {
		STFService stfService = new STFService(STF_SERVICE_URL, ACCESS_TOKEN);
		this.deviceApi = new DeviceApi(stfService);
		this.deviceApi.connectDevice(deviceSerial);
		isRemote = true;
	}

	public void releaseStfDevice() throws MalformedURLException, URISyntaxException {
		STFService stfService = new STFService(STF_SERVICE_URL, ACCESS_TOKEN);
		this.deviceApi = new DeviceApi(stfService);
		this.deviceApi.releaseDevice(deviceName);
	}

	public boolean isRemote() {
		return isRemote;
	}
}