package com.zemoga.page;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * This class provides and example on how to extend base page and use locators.
 */
public class LandingPage extends BasePage{

    @FindBy(id="email_edit_text")
    @AndroidFindBy(uiAutomator=ID_+"email_edit_text"+_ID)
    public WebElement eEmail;

    @FindBy(id="password_edit_text")
    @AndroidFindBy(uiAutomator=ID_+"password_edit_text"+_ID)
    public WebElement ePassword;

    @FindBy(id="button_login")
    @AndroidFindBy(uiAutomator=ID_+"button_login"+_ID)
    public WebElement eLogin;

    @FindBy(id="menu_account")
    @AndroidFindBy(uiAutomator=ID_+"menu_account"+_ID)
    public WebElement accountButton;

    @FindBy(id="snackbar_text")
    @AndroidFindBy(uiAutomator=ID_+"snackbar_text"+_ID)
    public WebElement snackbar;

    public LandingPage(WebDriver driver) {
        super(driver);
    }

    public void getIntoLoginPage(){
        this.click(accountButton);
    }
}